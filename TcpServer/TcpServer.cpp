#include "TcpServer.h"
#include "Debug.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "event2/thread.h"

#include <cstring>
#include <memory>
#include <signal.h>

const uint32_t g_QMsgMask = (QMSG_MASK);

/**
 * @fn
 * @brief
 * @param[in]
 * @param[out]
 * @retval
 */
TcpServer::TcpServer()
{
    /*
     *   If one end of the socket closes the connection, while the other endl
     * still writes data to it, it will receive an RST response after thefirst write of data.
     *   After writing data again, the kernel will send a SIGPIPE signal to
     * the process to notify the process that the connection has been disconnected.
     *   The default processing of the SIGPIPE signal is to terminate the program,
     * so here set ignores the SIGPIPE signal.
     */
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) 
    {
        DEBUG(LOG_ERROR, "Fail to signal.");
    }

    /**
     * 多线程环境下没有调用evthread_use_windows_threads或evthread_use_pthreads函数
     * 会导致event_base_dispatch函数一直阻塞，即使调用了event_base_loopbreak或event_base_loopexit
     * 也无法让event_base_dispatch退出事件循环。
     */
    evthread_use_pthreads();
}

/**
 * @fn
 * @brief
 * @param[in]
 * @param[out]
 * @retval
 */
TcpServer::~TcpServer()
{
    /* 停止监听 */
    stop();

    /* 释放资源 */
    unInit();
}

/**
 * @fn
 * @brief      释放资源
 * @param[in]
 * @param[out]
 * @retval    true:成功 false:失败
 */
bool TcpServer::unInit()
{
    if(m_evConnListener)
    {
        evconnlistener_free(m_evConnListener);
        m_evConnListener = nullptr;
    }

    if(m_base)
    {
        event_base_free(m_base);
        m_base = nullptr;
    }

    m_isInited = false;
    return true;
}

/**
 * @fn
 * @brief      初始化event base,绑定ip、端口
 * @param[in]
 * @param[out]
 * @retval     true:成功 false:失败
 */
bool TcpServer::init(std::string addr, int port)
{
    bool ret= true;
    sockaddr_in sin = {0};

    /* 上锁 */
    std::lock_guard<std::mutex> lock(m_mutex);

    if(m_isInited)
    {
        DEBUG(LOG_ERROR, "Alaredy inited, can not init again.");
        ret = false;
        goto EXIT;
    }

    m_addr = addr;
    m_port = port;

    sin.sin_family = AF_INET;
    sin.sin_port = htons(m_port);
    sin.sin_addr.s_addr = inet_addr(m_addr.c_str());

    /* 创建libevent的上下文 */
    m_base = event_base_new();
    if(!m_base)
    {
        DEBUG(LOG_ERROR, "Fail to event_base_new().\n");
        ret = false;
        goto EXIT;
    }

    m_evConnListener = evconnlistener_new_bind(m_base,                                   /* libevent的上下文                           */
                                                listenCallback,                          /* 接收到连接的回调函数                       */
                                                this,                                    /* 回调函数获取的参数 arg                     */
                                                LEV_OPT_REUSEABLE|LEV_OPT_CLOSE_ON_FREE, /* 地址重用，evconnlistener关闭同时关闭socket */
                                                30,                                      /* 连接队列大小，对应listen函数的backlog参数  */
                                                (sockaddr*)&sin,                         /* 绑定的地址和端口                           */
                                                sizeof(sin)
                                                );
    if(!m_evConnListener)
    {
        DEBUG(LOG_ERROR, "Fail to evconnlistener_new_bind().\n");
        ret = false;
        goto EXIT;
    }

    m_isInited = true;
    DEBUG(LOG_INFO, "Init Ok. ip:%s,port:%d\n", m_addr.c_str(), m_port);

EXIT:
    if(!ret)
    {
        if(m_evConnListener)
        {
            evconnlistener_free(m_evConnListener);
            m_evConnListener = nullptr;
        }

        if(m_base)
        {
            event_base_free(m_base);
            m_base = nullptr;
        }

    }
    return ret;
}

/**
 * @fn
 * @brief      bufferevent事件回调 
 * @param[in]
 * @param[out]
 * @retval
 */
void TcpServer::eventCallBack(struct bufferevent *bev, short what, void *arg)
{
    ClientArgs *pArg = reinterpret_cast <ClientArgs *>(arg);

    /* 如果对方网络断掉，或者机器死机有可能收不到BEV_EVENT_EOF数据 */
    if(what & BEV_EVENT_EOF)
    {
        DEBUG(LOG_DEBUG, "Disconnect. ip:%s,port:%d\n", inet_ntoa(pArg->sin.sin_addr), ntohs(pArg->sin.sin_port));
    }
    else if(what & BEV_EVENT_ERROR)
    {
        DEBUG(LOG_ERROR, "Error. ip:%s,port:%d\n", inet_ntoa(pArg->sin.sin_addr), ntohs(pArg->sin.sin_port));
    }
    else if(what & BEV_EVENT_TIMEOUT)
    {
        DEBUG(LOG_DEBUG, "Timeout. ip:%s,port:%d\n", inet_ntoa(pArg->sin.sin_addr), ntohs(pArg->sin.sin_port));
    }

    bufferevent_free(bev);
    delete pArg;
}

/**
 * @fn
 * @brief      bufferevent读回调函数
 * @param[in]
 * @param[out]
 * @retval
 */
void TcpServer::readCallBack(bufferevent * bev, void *arg)
{
    int32_t recvNum = 0;
    bool findHeadFlag = false;

    ClientArgs *pArg = reinterpret_cast <ClientArgs *>(arg);
    char * pData = nullptr;

    while(true)
    {
        QMessage msg = {0};
        if(pArg->m_currentRecv == RECV_HEAD)
        {
            /* 读取client发送的帧头 */
            recvNum = bufferevent_read(bev, &msg, sizeof(msg)-pArg->m_currentHeadLen);
            if(recvNum<=0)
            {
                break;
            }

            /* 完成接收到一个完整的head数据包 */
            if((recvNum == sizeof(msg)) && msg.isValid())
            {
                if(msg.dataLen != 0)
                {
                    pArg->m_head = msg;
                    pArg->m_currentRecv = RECV_BODY;
                    DEBUG(LOG_DEBUG, "recv a head, dataLen=%d\n", msg.dataLen);
                }
                else
                {
                    if(msg.cmd == E_QMsgCmd_HeartBeat)
                    {
                        DEBUG(LOG_DEBUG, "recv a heartBeat, dataLen=%d\n", msg.dataLen);
                        bufferevent_write(bev, &msg, sizeof(msg));
                    }
                    memset(&pArg->m_head, 0 , sizeof(pArg->m_head));
                }
                pArg->m_currentHeadLen = 0;
            }
            else
            {
                /**
                 * 如果之前没有找到head的mark，则查找mark
                 */
                if(!pArg->m_head.isValid())
                {
                    for(int32_t i = 0; i<recvNum; i++)
                    {
                        if(!memcmp((char *)(&msg) + i, &g_QMsgMask, sizeof(g_QMsgMask)))
                        {
                            findHeadFlag = true;
                            memcpy(&pArg->m_head, &msg+i, recvNum-i);
                            pArg->m_currentHeadLen = recvNum-i;
                            break;
                        }
                    }

                    if(!findHeadFlag)
                    {
                        DEBUG(LOG_WARN, "recv a trash pack, no mask in pack\n");
                    }
                    else
                    {
                        DEBUG(LOG_WARN, "find head in pack middle\n");
                    }
                }
                else
                {
                    /* 接收后半个head */
                    if(recvNum == (int)sizeof(msg) - pArg->m_currentHeadLen)
                    {
                        memcpy(&pArg->m_head + pArg->m_currentHeadLen, &msg, recvNum);

                        DEBUG(LOG_DEBUG, "finish recv a head, dataLen=%d\n", pArg->m_head.dataLen);
                        if(pArg->m_head.dataLen != 0)
                        {
                            pArg->m_currentRecv = RECV_BODY;
                        }
                        else
                        {
                            memset(&pArg->m_head, 0 , sizeof(pArg->m_head));
                        }
                    }
                    else
                    {
                        DEBUG(LOG_ERROR, "fail to recv the second half head pack\n");
                        memset(&pArg->m_head, 0 ,sizeof(pArg->m_head));
                    }

                    pArg->m_currentHeadLen = 0;
                }
            }
        }

        /**
         * 接收body
         */
        else if(pArg->m_currentRecv == RECV_BODY)
        {
            const int bufLen = 1024;
            int needRecvLen = pArg->m_head.dataLen - pArg->m_currentBodyLen;
            int allocLen = needRecvLen < bufLen ? needRecvLen : bufLen;

            pData = new char[allocLen];
            if(pData == NULL)
            {
                DEBUG(LOG_ERROR, "Allocate memory err:len=%d\n", pArg->m_head.dataLen);
                goto EXIT;
            }

            memset(pData, 0, allocLen);

            recvNum = bufferevent_read(bev, pData, allocLen);
            if(recvNum<=0)
            {
                break;
            }

            DEBUG(LOG_DEBUG, "recv body data :len=%d\n", recvNum);
            if(pArg->fp == nullptr)
            {
                if((pArg->fp = fopen("./1.jpg", "w+")) == nullptr)
                {
                    DEBUG(LOG_ERROR, "fail to open file\n");
                }
            }

            {
                int writeFileSize = fwrite(pData, 1, recvNum, pArg->fp);
                if(writeFileSize != recvNum)
                {
                    DEBUG(LOG_ERROR, "fail to wirte file\n");
                }
            }
            pArg->m_currentBodyLen += recvNum;
            if( (uint32_t)pArg->m_currentBodyLen == pArg->m_head.dataLen)
            {
                DEBUG(LOG_DEBUG, "recv package data finished:total len=%d\n", pArg->m_currentBodyLen);
                pArg->m_currentRecv = RECV_HEAD;
                pArg->m_currentBodyLen = 0;
                pArg->m_currentHeadLen = 0;
                memset(&pArg->m_head, 0 , sizeof(pArg->m_head));
                if(pArg->fp != nullptr)
                {
                    fclose(pArg->fp);
                    pArg->fp =nullptr;
                }
            }

            delete[] pData;
            pData = nullptr;
        }
    }
    /* 调用注册的回调函数处理消息 */

EXIT:
    if(pData)
    {
        delete[] pData;
        pData = nullptr;
    }
}

/**
 * @fn
 * @brief      监听到新连接的回调函数
 * @param[in]
 * @param[out]
 * @retval
 */
void TcpServer::listenCallback(struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *addr, int len, void *ptr)
{
    struct sockaddr_in* client = (sockaddr_in*)addr;
    DEBUG(LOG_DEBUG, "New connect, client ip:%s,port:%d\n", inet_ntoa(client->sin_addr), ntohs(client->sin_port));

    TcpServer *pThis = reinterpret_cast<TcpServer *>(ptr);

    bufferevent *bev = bufferevent_socket_new(pThis->m_base, fd, BEV_OPT_CLOSE_ON_FREE);

    ClientArgs *pArg = new ClientArgs;
    pArg->pThis = pThis;
    memcpy(&pArg->sin, client, sizeof(struct sockaddr_in));

    bufferevent_setcb(bev, readCallBack, NULL, eventCallBack, pArg);

    bufferevent_enable(bev, EV_READ);

    /* 添加超时 */
    timeval val = {120, 0};
    bufferevent_set_timeouts(bev, &val, 0);
}

/**
 * @fn
 * @brief     启动监听新连接
 * @param[in]
 * @param[out]
 * @retval    true:成功 false:失败
 */
bool TcpServer::start()
{
    bool ret = true;

    /* 上锁 */
    std::lock_guard<std::mutex> lock(m_mutex);

    if(!m_isInited)
    {
        DEBUG(LOG_ERROR, "Start error, tcp Server no init.\n");
        ret = false;
        goto EXIT;
    }

    if(m_isRunning)
    {
        DEBUG(LOG_ERROR, "Alaredy start, can not start again..\n");
        ret = false;
        goto EXIT;
    }

    /* 设置运行标志位在创建线程之前 */
    m_isRunning = true;

    /* 创建IO线程 */
    if(pthread_create(&m_threadID, NULL, pollThread, this) != 0)
    {
        DEBUG(LOG_ERROR, "pthread_create failed!.\n");
        m_isRunning = false;
        ret = false;
        goto EXIT;
    }
    DEBUG(LOG_INFO, "Start Ok. ip:%s,port:%d\n", m_addr.c_str(), m_port);

EXIT:
    return ret;
}

/**
 * @fn
 * @brief     停止监听新连接
 * @param[in]
 * @param[out]
 * @retval    true:成功 false:失败
 */
bool TcpServer::stop()
{
    void *thrdRet = NULL;

    /* 上锁 */
    std::lock_guard<std::mutex> lock(m_mutex);

    if(m_isRunning)
    {
        m_isRunning = false;
        event_base_loopbreak(m_base);
        pthread_join(m_threadID, &thrdRet);
        DEBUG(LOG_INFO, "Stop. ip:%s,port:%d\n", m_addr.c_str(), m_port);
    }
    return (m_isRunning == false);
}

/**
 * @fn
 * @brief     dispatch线程函数
 * @param[in]
 * @param[out]
 * @retval  void *
 */
void *TcpServer::pollThread(void*arg)
{
    TcpServer * pThis = reinterpret_cast<TcpServer*>(arg);

    while(pThis->m_isRunning)
    {
        if(pThis->m_base)
        {
            event_base_dispatch(pThis->m_base);
        }
    }

    DEBUG(LOG_INFO, "Tcp server poll thread exit.\n");
    return NULL;
}

