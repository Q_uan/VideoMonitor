#ifndef _TCPSERVER_H_
#define _TCPSERVER_H_

#include <event2/event.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>

#include <functional>
#include <pthread.h>
#include <string>
#include <mutex>

#include "QMessage.h"

class TcpServer
{
public:
public:
    TcpServer();
    ~TcpServer();
    bool init(std::string addr, int port);            /* 初始化 */
    bool start();                                     /* 启动监听新连接 */
    bool stop();                                      /* 停止监听新连接 */
private:
    TcpServer(const TcpServer&);                      /* 禁止外部调用拷贝构造函数 */
    TcpServer& operator= (const TcpServer&);          /* 禁用赋值操作符 */
    bool unInit();                                    /* 释放资源 */
    static void listenCallback(struct evconnlistener * e, evutil_socket_t s, struct sockaddr *a, int socklen, void *arg);/* 监听到新连接的回调函数 */
    static void readCallBack(bufferevent * bev, void *arg);                   /* bufferevent读回调函数 */
    static void eventCallBack(struct bufferevent *bev, short what, void *arg);/* bufferevent事件回调 */
    static void* pollThread(void*arg);                                        /* dispatch线程函数 */
private:
    std::string  m_addr;    /* ip */
    int m_port = 0;         /* 端口号 */

    event_base* m_base = nullptr;
    evconnlistener * m_evConnListener = nullptr;

    pthread_t m_threadID = 0;                /* 线程ID */
    volatile bool m_isRunning = false;       /* 运行标志 */
    volatile bool m_isInited = false;        /* 初始化标志 */
    std::mutex m_mutex;

    typedef enum
    {
        RECV_HEAD,
        RECV_BODY,
    }E_RECV_TYPE;
private:
	struct ClientArgs
	{
		E_RECV_TYPE m_currentRecv = RECV_HEAD; /* 默认接收数据头 */
		int m_currentHeadLen = 0;              /* 存放当前接收到的head长度 */
		int m_currentBodyLen = 0;              /* 存放当前接收到的body长度 */
		QMessage m_head{};                     /* 存放数据head */
		TcpServer *pThis = nullptr;
		sockaddr_in sin{};
		FILE *fp= nullptr;
	};
};
#endif
