#include "Debug.h"
#include "TcpServer.h"
#include <unistd.h>

int main(int argc, const char *argv[])
{
	TcpServer server;
	server.init("0.0.0.0", 8888);
	server.start();

	while(1)
	{
		sleep(5);
	}
	server.stop();

	return 0;
}
