#ifndef _QMESSAGE_H_
#define _QMESSAGE_H_

#include <stdint.h>

/* 帧头值 */
#define QMSG_MASK (0XFFAA55FF)
typedef enum
{
	E_QMsgCmd_HeartBeat = 100, /* 心跳包 */
	E_QMsgCmd_PicTrans         /* 图片传输 */

}QMessageCmd;
/**
 * 通讯协议
 */
#pragma pack(4)
struct QMessage
{
public:
	uint32_t   mask;          /* 帧头 */
    uint32_t   cmd;           /* 命令type */
	uint32_t   reserve;       /* 保留字节 */
    uint32_t   dataLen;       /* 负载数据长度 */ 
    int8_t     payload[0]; 
public:
	bool isValid()
	{
		return (this->mask == QMSG_MASK);
	}
	void init()
	{
		this->mask = QMSG_MASK;
	}
};
#pragma pack()

#endif
