#ifndef __DEBUG_H_
#define __DEBUG_H_
#include <cstdio>
#include <time.h>
enum LOG_LEVEL
{
    LOG_DISABLE = 0,
    LOG_ERROR,
    LOG_WARN,
    LOG_INFO,
    LOG_DEBUG
};

#define PRINT_RED               "\033[1;31m"
#define PRINT_GREEN             "\033[1;32m"
#define PRINT_NONE              "\033[0m"

#define PIRNT_LEVEL  (LOG_DEBUG)      /* 小于等于这个级别的日志将被打印 */
#define DEBUG(level, ...)  do{ \
                                        if( level <= PIRNT_LEVEL) \
                                        {\
											char buf[] = {' ', 'E', 'W', 'I', 'D', ' '};\
                                            time_t t = 0;\
                                            struct tm stm = {0};\
                                            t = time(NULL);\
                                            localtime_r(&t, &stm);\
                                            printf(PRINT_RED "%c[%04d/%02d/%02d %02d:%02d:%02d]",buf[level],\
                                                (stm.tm_year + 1900), (1 + stm.tm_mon), stm.tm_mday, stm.tm_hour, stm.tm_min, stm.tm_sec);\
                                            printf(PRINT_RED " %s:%d" PRINT_GREEN " %s():" PRINT_NONE,__FILE__,__LINE__,__FUNCTION__);\
                                            printf(__VA_ARGS__);\
                                        }\
                                    }while(0)

#endif

