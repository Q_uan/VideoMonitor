#include "TcpClient.h"
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>


#define ALARMDEVTCPCLIENT_CONNECT_TIMEOUT_SEC  (10) /* socket 连接超时*/
#define HEARTBEATCYCLE_SEC   (10) /* 主动发送心跳的周期（秒） */
const uint32_t g_QMsgMask = (QMSG_MASK);

/**
 * @fn        TcpClient::TcpClient
 * @brief     构造函数
 * @param[in]
 * @param[out]
 * @retval
 */
TcpClient::TcpClient()
{

}

/**
 * @fn        TcpClient::~TcpClient
 * @brief     析构函数，退出线程，资源释放
 * @param[in]
 * @param[out]
 * @retval
 */
TcpClient::~TcpClient()
{
    stop();
    for(int i = 0; i<2; i++)
    {
        if(m_pipeFd[i] != -1)
        {
            close(m_pipeFd[i]);
        }
    }
}

/**
 * @fn         TcpClient::init
 * @brief      初始化
 * @param[in]  addr：tcp server ip地址
               port：tcp server 端口号
 * @param[out]
 * @retval    成功：true，失败：false
 */
bool TcpClient::init(std::string addr, int port)
{
    bool ret = true;

    /* 创建管道 */
    if(pipe(m_pipeFd) < 0)
    {
        DEBUG(LOG_ERROR, "Fail to create pipe.");
        ret = false;
    }

    m_serverPort = port;
    m_serverAddr = addr;

    return ret;
}

/**
 * @fn         TcpClient::start
 * @brief      启动线程，尝试连接tcpServer并监听接收报警消息
 * @param[in]  
 * @param[out]
 * @retval    成功：true，失败：false
 */
bool TcpClient::start()
{
    bool ret = true;
    if(m_isRunning)
    {
        DEBUG(LOG_ERROR, "Alaredy start, can not start again..\n");
        ret = false;
        goto EXIT;
    }

    /* 设置运行标志位在创建线程之前 */
    m_isRunning = true;

    /* 创建IO线程 */
    if(pthread_create(&m_threadID, NULL, pollThread, this) != 0)
    {
        DEBUG(LOG_ERROR, "pthread_create failed!.\n");
        m_isRunning = false;
        ret = false;
        goto EXIT;
    }
EXIT:
    return ret;
}

/**
 * @fn         TcpClient::stop
 * @brief      停止socket接收线程
 * @param[in]  
 * @param[out]
 * @retval    成功：true，失败：false
 */
bool TcpClient::stop()
{
    void *thrdRet = NULL;
    Message pipeMsg;

    if(m_isRunning)
    {
        m_isRunning = false;
        write(m_pipeFd[1], (char *)&pipeMsg, sizeof(pipeMsg));
        pthread_join(m_threadID, &thrdRet);
        DEBUG(LOG_INFO, "TcpClient Stop.\n");
    }
    return (m_isRunning == false);
}

/**
 * @fn         TcpClient::setReadHandler
 * @brief      设置报警消息回调函数
 * @param[in]  
 * @param[out]
 * @retval    成功：true，失败：false
 */
bool TcpClient::setReadHandler(ReadHandler readCb)
{
    if(readCb)
    {
        m_readHandler = readCb;
    }
    return (m_readHandler != nullptr);
}

/**
 * @fn          TcpClient::connectSocket
 * @brief       建立与tcp server之间的socket连接
 * @param[in]   
 * @param[out]
 * @retval      true：成功 false：失败
 */
bool TcpClient::connectSocket()
{
    bool ret = true;
    int maxfd = -1;
    int conRet = 0;
    Message pipeMsg = {0};
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(m_serverPort);
    sin.sin_addr.s_addr = inet_addr(m_serverAddr.c_str());
    int flags = 0;

    /**
     * socket当前已经可用，不能重复连接
     */
    if(-1 != m_sockFd)
    {
        DEBUG(LOG_ERROR, "m_sockFd is in used.\n");
        ret = false;
        goto EXIT;
    }

    /**
     * 创建socket
     */
    m_sockFd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(m_sockFd < 0)
    {
        m_sockFd = -1;
        DEBUG(LOG_ERROR, "Fail to create socket.\n");
        ret = false;
        goto EXIT;
    }

    /**
     * 设定socket为非阻塞模式
     */
    flags = fcntl(m_sockFd, F_GETFL);
    if(flags == -1)
    {
        DEBUG(LOG_ERROR, "Fail to fcntl socket.\n");
        ret = false;
        goto EXIT;
    }
    flags |= O_NONBLOCK;
    if(0 != fcntl(m_sockFd, F_SETFL, flags))
    {
        DEBUG(LOG_ERROR, "Fail to fcntl socket.\n");
        ret = false;
        goto EXIT;
    }

    /**
     * 实现非阻塞 connect ，首先把 sockfd 设置成非阻塞的。这样调用 connect 可以立刻返回，根据返回值和 errno 处理三种情况：
     * (1) 如果返回 0，表示 connect 成功。
     * (2) 如果返回值小于 0，errno 为 EINPROGRESS, 表示连接建立已经启动但是尚未完成。这是期望的结果，不是真正的错误。
     * (3) 如果返回值小于0，errno 不是 EINPROGRESS，则连接出错了。
     */
    conRet = connect(m_sockFd, (struct sockaddr*)&sin, sizeof(sin));
    if(conRet < 0)
    {
        /**
         * 判断errno是否为EINPROGRESS
         */
        if(errno != EINPROGRESS)
        {
            DEBUG(LOG_ERROR, "conect to socket error.\n");
            ret = false;
            goto EXIT;
        }

        struct timeval timeoutVal;
        timeoutVal.tv_sec = ALARMDEVTCPCLIENT_CONNECT_TIMEOUT_SEC;
        timeoutVal.tv_usec = 0;

        fd_set readSet, writeSet;
        FD_ZERO(&readSet);
        FD_ZERO(&writeSet);

        FD_SET(m_sockFd, &readSet);
        FD_SET(m_pipeFd[0], &readSet);
        FD_SET(m_sockFd, &writeSet);

        maxfd = m_pipeFd[0] > m_sockFd ? m_pipeFd[0] : m_sockFd;

        /**
         * 把sockfd加入select的读写监听集合，通过select判断 sockfd是否可写，处理三种情况：
         * (1) 如果连接建立好了，对方没有数据到达，那么 sockfd 是可写的
         * (2) 如果在 select 之前，连接就建立好了，而且对方的数据已到达，那么sockfd是可读和可写的。
         * (3) 如果连接发生错误，sockfd 也是可读和可写的。
         * 判断 connect 是否成功，就得区别 (2) 和 (3)，这两种情况下sockfd都是可读和可写的，区分的方法是，调用 getsockopt 检查是否出错。
         */
        int retCon = select(maxfd+1, &readSet, &writeSet, NULL, &timeoutVal);
        if(retCon > 0)
        {
            /**
             * 可读
             */
            if(FD_ISSET(m_sockFd, &readSet))
            {
                /**
                 * 使用getsockopt函数检查错误：
                 * 在 sockfd 都是可读和可写的情况下，我们使用 getsockopt 来检查连接是否出错。
                 * 但这里有一个可移植性的问题。如果发生错误，getsockopt 源自 Berkeley 的实现将在变量 error 中返回错误，getsockopt 本身返回0；
                 * 然而 Solaris 却让 getsockopt 返回 -1，并把错误保存在 errno 变量中。所以在判断是否有错误的时候，要处理这两种情况。
                 */
                int error = 0, code = 0;
                socklen_t len = 0;
                len = sizeof(error);
                code = getsockopt(m_sockFd, SOL_SOCKET, SO_ERROR, &error, &len);
                if(code < 0 || error)
                {
                    DEBUG(LOG_ERROR, "conect to socket error.\n");
                    ret = false;
                    goto EXIT;
                }
                FD_CLR(m_sockFd, &readSet);
            }

            /**
             * 可写
             */
            if(FD_ISSET(m_sockFd, &writeSet))
            {
                DEBUG(LOG_DEBUG, "connect to %s:%d ok.\n", m_serverAddr.c_str(),  m_serverPort);
                FD_CLR(m_sockFd, &writeSet);
            }

            /**
             * 收到管道消息，消息没有作用，仅仅是为了从select阻塞中退出
             */
            if(FD_ISSET(m_pipeFd[0], &readSet))
            {
                FD_CLR(m_pipeFd[0], &readSet);
                read(m_pipeFd[0],&pipeMsg, sizeof(pipeMsg));
		if(pipeMsg.event = E_EXIT)
		{
                    DEBUG(LOG_DEBUG, "recv a exit message.\n");
                    ret = false;
                    goto EXIT;
		}
		else if(pipeMsg.event = E_SEND)
		{
                    DEBUG(LOG_DEBUG, "recv a send message.\n");
                    ret = false;
                    goto EXIT;

		}
            }
        }
        /**
         * 连接超时
         */
        else if(retCon == 0)
        {
            DEBUG(LOG_DEBUG, "connect to %s:%d timeout.\n", m_serverAddr.c_str(),  m_serverPort);
            ret = false;
            goto EXIT;
        }
        /**
         * 异常错误
         */
        else
        {
            DEBUG(LOG_ERROR, "fail to select, thread will exit.\n");
            ret = false;
            goto EXIT;
        }
    }

EXIT:
    if(!ret)
    {
        close(m_sockFd);
        m_sockFd = -1;
    }
    return ret;
}

/**
 * @fn          TcpClient::setConnectPara
 * @brief       设置连接参数
 * @param[in]   
 * @param[out]
 * @retval      true：成功 false：失败
 */
bool TcpClient::setConnectPara()
{
    bool ret = true;
    int bKeepAlive = 1;
    int flags = 0;

    /* socket不可用，退出 */
    if(-1 == m_sockFd)
    {
        ret = false;
        goto EXIT;
    }

    /* 设定socket为阻塞模式 */
    flags = fcntl(m_sockFd, F_GETFL);
    if(flags == -1)
    {
        DEBUG(LOG_ERROR, "Fail to fcntl socket.\n");
        ret = false;
        goto EXIT;
    }
    flags  &=~ O_NONBLOCK;
    if(0 != fcntl(m_sockFd, F_SETFL, flags))
    {
        DEBUG(LOG_ERROR, "Fail to fcntl socket.\n");
        ret = false;
        goto EXIT;
    }

    /* 设置为长连接 */
    setsockopt(m_sockFd, SOL_SOCKET, SO_KEEPALIVE, (const char* )&bKeepAlive, sizeof(bKeepAlive));

EXIT:
    if(!ret)
    {
        close(m_sockFd);
        m_sockFd = -1;
    }
    return ret;
}

/**
 * @fn           TcpClient::recvData
 * @brief        接收tcp server发来的消息
 * @param[in]
 * @param[out]
 * @retval
 */
void TcpClient::recvData()
{
    bool ret = true;
    int recvNum = 0;
    char * pData = nullptr;
    QMessage tempHeader = {0};
    bool findHeadFlag = false;

    /**
     * 接收head
     */
    if(m_currentRecv == RECV_HEAD)
    {
        recvNum = recv(m_sockFd, &tempHeader, sizeof(tempHeader)-m_currentHeadLen, 0);

        /**
         * 连接断开
         */
        if(recvNum <= 0) 
        {
            DEBUG(LOG_ERROR, "disconnect \n");
            ret = false;
            goto EXIT;
        }

        /**
         * 完成接收到一个完整的head数据包
         */
        if((recvNum == (int)sizeof(tempHeader)) && tempHeader.isValid())
        {
            m_head = tempHeader;

            /** 
             * 如果body的长度不为0，接收body
             */
            if(tempHeader.dataLen != 0)
            {
                m_currentRecv = RECV_BODY;
            }
			DEBUG(LOG_INFO,"recv a pack\n");
            m_currentHeadLen = 0;
        }
        else
        {
            /**
             * 接收到小于head长度的数据包
             * 如果之前没有找到head的mark，则查找mark
             */
            if(!tempHeader.isValid())
            {
                for(int i = 0; i<recvNum; i++)
                {
                    if(!memcmp((char* )&tempHeader + i, &g_QMsgMask, sizeof(g_QMsgMask)))
                    {
                        findHeadFlag = true;
                        memcpy(&m_head, &tempHeader+i, recvNum-i);
                        m_currentHeadLen = recvNum-i;
                        break;
                    }
                }

                if(!findHeadFlag)
                {
                    DEBUG(LOG_WARN, "recv a trash pack, no mark in pack\n");
                }
            }
            else
            {
                /**
                 * 接收后半个head
                 */
                if(recvNum == (int)sizeof(tempHeader)-m_currentHeadLen)
                {
                    memcpy(&m_head + m_currentHeadLen, &tempHeader, recvNum);
                    if(m_head.dataLen != 0)
                    {
                        m_currentRecv = RECV_BODY;
                    }
                }
                else
                {
                    DEBUG(LOG_ERROR, "fail to recv the second half head pack\n");
                    memset(&m_head, 0 ,sizeof(m_head));
                }

                m_currentHeadLen = 0;
            }
        }
    }

    /**
     * 接收body
     */
    else if(m_currentRecv == RECV_BODY)
    {
        pData = new char[m_head.dataLen];

        if(nullptr == pData)
        {
            DEBUG(LOG_ERROR, "No enough memory to new\n");
            ret = false;
            goto EXIT;
        }
        memset(pData, 0, m_head.dataLen);

        recvNum = recv(m_sockFd, pData, m_head.dataLen - m_currentBodyLen, 0);

        /**
         * 连接断开
         */
        if(recvNum <= 0)
        {
            DEBUG(LOG_ERROR, "disconnect recvNum=%d\n", recvNum);
            ret = false;
            goto EXIT;
        }

        memcpy(&m_body + m_currentBodyLen, pData, recvNum);
        m_currentBodyLen += recvNum;

        if((int)m_head.dataLen == m_currentBodyLen)
        {
            /**
             * 解析并处理报警消息
             */

			//TODO...

            m_currentBodyLen = 0;
            memset(&m_body, 0 ,sizeof(m_body));
            memset(&m_head, 0 ,sizeof(m_head));
            m_currentRecv = RECV_HEAD;
        }
    }

EXIT:
    if(pData)
    {
        delete[] pData;
        pData = nullptr;
    }

    if(!ret)
    {
        if(m_sockFd != -1)
        {
            close(m_sockFd);
            m_sockFd = -1;
        }
    }
}

/**
 * @fn          TcpClient::pollThread
 * @brief       接收报警消息的线程函数
                收到心跳消息和报警消息都证明网络正常
 * @param[in]   arg：指向TcpClient类型的指针
 * @param[out]
 * @retval      void *
 */
void *TcpClient::pollThread(void*arg)
{
    fd_set readSet = {0};
    int selectRet = 0;
    TcpClient * pThis = reinterpret_cast<TcpClient*>(arg);
    int maxfd = pThis->m_pipeFd[0];
    Message pipeMsg = {0};
    timeval timeoutVal = {HEARTBEATCYCLE_SEC, 0};

    while(pThis->m_isRunning)
    {
        /* 尝试和tcp server建立连接并设置参数 */
        if(-1 == pThis->m_sockFd)
        {
            bool ret = true;
            ret = ret && pThis->connectSocket(); /* 尝试建立连接 */
            ret = ret && pThis->setConnectPara();/* 设置参数 */
            if(!ret)
            {
                DEBUG(LOG_INFO, "Fail to setup connect %s:%d.\n", pThis->m_serverAddr.c_str(),  pThis->m_serverPort);
            }
            if(!pThis->m_isRunning)/* 检查连接过程中是否进行了退出操作 */
            {
                break;
            }
        }

        /* 清空集合 */
        FD_ZERO(&readSet);

        /* 将需要的文件描述符添加到集合里面 */
        FD_SET(pThis->m_pipeFd[0], &readSet);
        if(-1 != pThis->m_sockFd)
        {
            /* 连接成功则监听socket和管道消息，并将超时时间设置为心跳周期加10s */
            timeoutVal = {HEARTBEATCYCLE_SEC, 0};
            FD_SET(pThis->m_sockFd, &readSet);
            maxfd = pThis->m_pipeFd[0] > pThis->m_sockFd ? pThis->m_pipeFd[0] : pThis->m_sockFd;
        }
        else
        {
            /* 连接失败则只监听管道，并将超时时间设置为3s */
            timeoutVal = {5, 0};
            maxfd = pThis->m_pipeFd[0];
        }

        /* select监听 */
        selectRet = select(maxfd + 1, &readSet, NULL, NULL, &timeoutVal);

        /* 超时,心跳周期内没收到心跳包，关闭socket */
        if(0 == selectRet)
        {
			pThis->doSendHeartBeat();
			
	
			//		pThis->doSendPic();

        }
        else if(selectRet > 0)
        {
            if(FD_ISSET(pThis->m_sockFd, &readSet))
            {
                pThis->recvData();
                FD_CLR(pThis->m_sockFd, &readSet);
            }

            /* 管道收到的消息没有作用，仅仅是为了通过管道通知线程从睡眠中退出 */
            if(FD_ISSET(pThis->m_pipeFd[0], &readSet))
            {
                read(pThis->m_pipeFd[0], &pipeMsg, sizeof(pipeMsg));
				if(pipeMsg.event == E_EXIT)
				{
                	DEBUG(LOG_DEBUG, "TcpClient::pollThread recv exit message.\n");
				}
				else if(pipeMsg.event == E_SEND)
				{
					pThis->doSendPic();
				}

                FD_CLR(pThis->m_pipeFd[0], &readSet);
            }
        }
        else
        {
            DEBUG(LOG_ERROR, "fail to select.\n");
            continue;
        }
    }

    /* 退出线程前，回收资源 */
    if(pThis->m_sockFd != -1)
    {
        close(pThis->m_sockFd);
        pThis->m_sockFd = -1;
    }

    DEBUG(LOG_INFO, "TcpClient poll thread exit.\n");
    return NULL;
}

bool TcpClient::sendPicture()
{
    Message pipeMsg;

	if(m_sockFd == -1)
	{
		m_isBusy = false;
		return false;
	}

	if(m_isBusy)
	{
		return false;
	}

	m_isBusy = true;

	pipeMsg.event = E_SEND;
    write(m_pipeFd[1], (char *)&pipeMsg, sizeof(pipeMsg));

	return true;
}

bool TcpClient::doSendHeartBeat()
{
	bool ret = true;
	QMessage sendMsg{};
	int nWriteSize = 0;

	if(m_sockFd == -1)
	{
		ret = false;
    	DEBUG(LOG_INFO, "net err, do not send heartbeat .\n");
		goto EXIT;
	}

	if(m_isBusy)
	{
		goto EXIT;
	}

	sendMsg.init();
	sendMsg.cmd = E_QMsgCmd_HeartBeat;

    nWriteSize = send(m_sockFd, &sendMsg, sizeof(sendMsg), 0);
    if(nWriteSize != sizeof(sendMsg))
    {
        DEBUG(LOG_DEBUG, "send err\n");
        ret = false;
        goto EXIT;
    }

	m_isBusy = false;

    DEBUG(LOG_INFO, "do send heartbeat finished.\n");
EXIT:
	return ret;
}


bool TcpClient::doSendPic()
{
	bool ret = true;
	QMessage sendMsg;
	int nWriteSize = 0;
	int nReadFileSize = 0;
	FILE*fp = nullptr;
	
	char fileBuf[1024] = {0};

	if(m_sockFd == -1)
	{
	    m_isBusy = false;
		ret = false;
    	DEBUG(LOG_INFO, "net err, do not send pic .\n");
		goto EXIT;
	}
	if(m_isBusy == false)
	{
		goto EXIT;
	}

	sendMsg.init();
	sendMsg.cmd = E_QMsgCmd_PicTrans;

	if((fp=fopen("/tmp/quan.jpg", "r")) == nullptr)
	{
        DEBUG(LOG_DEBUG, "fail to open file\n");
        ret = false;
		goto EXIT;
	}
	fseek(fp,0,SEEK_END);
	sendMsg.dataLen = ftell(fp);
	fseek(fp,0,SEEK_SET);

	/* 发送帧头 */
    nWriteSize = send(m_sockFd, &sendMsg, sizeof(sendMsg), 0);
    if(nWriteSize != sizeof(sendMsg))
    {
        DEBUG(LOG_DEBUG, "send err\n");
        ret = false;
        goto EXIT;
    }

	while((nReadFileSize = fread(fileBuf, 1 , sizeof(fileBuf), fp))>0)
	{
		nWriteSize = send(m_sockFd, fileBuf, nReadFileSize, 0);
		if(nWriteSize != nReadFileSize)
		{
			DEBUG(LOG_DEBUG, "send err\n");
			ret = false;
			goto EXIT;
		}
        //DEBUG(LOG_DEBUG, "nWriteSize=%d ....\n", nWriteSize);
	}
	m_isBusy = false;

    DEBUG(LOG_DEBUG, "do send pic finished.\n");
EXIT:
	if(fp != nullptr)
	{
		fclose(fp);
	}
	return ret;
}

