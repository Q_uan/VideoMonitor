#include "Debug.h"
#include "TcpClient.h"
#include <unistd.h>

#include<opencv2/opencv.hpp>
#include <iostream>

#include <stdio.h>
#include <sys/time.h> // for gettimeofday()
#include <string.h>   // for memset()
#include <list>
#include <string>

using namespace std;
using namespace cv;

int main(int argc, const char *argv[])
{
	TcpClient client;

	client.init("123.57.92.179", 8888);
	//client.init("192.168.11.156", 8888);
	client.start();


	VideoCapture capture;

        capture.open(0);
	//capture.open("rtsp://admin:admin123456@192.168.60.18:554/main");
	if(capture.isOpened())
	{
		cout<<"success to  open video"<<endl;
	}
	else
	{
		cout<<"fail to open video"<<endl;
		return -1;
	}

	Mat img;//定义一个Mat变量，用于存储每一帧的图像
	capture>>img;//读取当前帧

	int fps = capture.get(CAP_PROP_FPS);
	cout<<"fps="<<fps<<endl;
	cout<<"图像数据大小(bytes):"<<img.rows * img.cols * img.elemSize()<<endl;/* 图像数据大小(bytes) */
	cout<<"图像宽 width = "<<img.size().width<<endl;                         /* 图像宽 */
	cout<<"图像高 height = "<<img.size().height<<endl;                       /* 图像高 */

	Mat frame;//定义一个Mat变量，用于存储每一帧的图像
	while(capture.isOpened())
	{
		capture>>frame;//读取当前帧

		resize(frame, img, Size(frame.cols/8, frame.rows/8),0,0,INTER_LINEAR);  
		if(!client.isBusy())
		{
		        imwrite("/tmp/quan.jpg", img);
			client.sendPicture();
		}
		else
		{
			sleep(5);
		}
		usleep(1000*50);
	}
	client.stop();
	capture.release();

	return 0;
}
