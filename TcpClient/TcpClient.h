#ifndef _TCPCLIENT_H_
#define _TCPCLIENT_H_

#include <functional>
#include <pthread.h>
#include <string>
#include <mutex>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Debug.h"
#include "QMessage.h"

using std::string;

class TcpClient
{
public:
    using ReadHandler = std::function<void (QMessage event)>;/* 设置读取数据回调函数 */
public:
    TcpClient();
    ~TcpClient();
    bool init(std::string addr, int port);    /* 初始化 */
    bool start();                             /* 启动监听新连接 */
    bool stop();                              /* 停止监听新连接 */
    bool setReadHandler(ReadHandler readCb);  /* 设置读到报警消息的回调函数 */
public:
	bool isBusy() {return m_isBusy;}
	bool sendPicture();
private:
	bool doSendPic();
	bool doSendHeartBeat();
private:
    int m_sockFd = -1;
private:
    bool m_isRunning = false;
    pthread_t m_threadID = 0;                 /* 线程ID */
    static void* pollThread(void*arg);        /* 线程函数 */

    bool connectSocket();                     /* 尝试连接socket */
    bool setConnectPara();                    /* 设置连接参数 */
    void recvData();
private:
    typedef enum
    {
        RECV_HEAD,
        RECV_BODY,
    }E_RECV_TYPE;
    E_RECV_TYPE m_currentRecv = RECV_HEAD; /* 默认接收数据头 */

	bool m_isBusy = false;
    int m_currentHeadLen = 0;              /* 存放当前接收到的head长度 */
    int m_currentBodyLen = 0;              /* 存放当前接收到的body长度 */
    QMessage m_head{};                     /* 存放数据head */
	char m_body[256]{};
private:
    int m_serverPort = 0;
    std::string m_serverAddr;
private:
    int m_pipeFd[2] = {-1, -1};               /* 这个管道用于通知线程从select超时中退出 */
	enum PipeEvent
	{
		E_EXIT = 0,
		E_SEND =1
	};
    struct Message
    {
        uint32_t event ;
    };
private:
    ReadHandler m_readHandler = nullptr;      /* 用于存放回调函数指针 */
};

#endif

